from nbp_api_app.utils import DateHandler
from nbp_api_app.api_handler import GoldPrice, GoldQueryEnum
print('----- This is my termial simple app for extracting and modyfing data from NBP api -----')

golden_query = GoldPrice()

while True:
    print('\nWhat would you like to check?')
    print(f'''
    a. Current price of gold
    b. Last 10 records of gold price
    c. Gold price on a given date
    d. Gold price in given time period
    e. Change unit for gold price enquiries (currently: {golden_query.mass_unit})
    f. Change currency for gold price enquiry (currently: {golden_query.currency})
    q. Quit
''')
    user_response = input('>>>').upper()

    if user_response == 'A':
        golden_query.send_query(GoldQueryEnum.CURRENT_PRICE.value)

    elif user_response == 'B':
        golden_query.send_query(GoldQueryEnum.TOP_PRICES.value)
    
    elif user_response == 'C':
        date = DateHandler.get_date()
        if date: golden_query.send_query(GoldQueryEnum.PRICE_ON_DATE.value, date=date)
    
    elif user_response == 'D':
        dates = DateHandler.get_dates_range()
        if dates: golden_query.send_query(GoldQueryEnum.PRICES_FROM_DATES_RANGE.value, \
            start_date = dates['start_date'], end_date = dates['end_date'])
    
    elif user_response == 'E':
        golden_query.change_reporting_unit()
    
    elif user_response == 'F':
        golden_query.change_reporting_currency()
    
    elif user_response == 'Q':
        break

golden_query.excel_writer.save_queries()
print('\n----- Good bye! -----\n')
