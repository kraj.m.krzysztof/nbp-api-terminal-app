import requests
from enum import Enum
from .utils import ResponseUtils, DataConverter, JSONReader
from .excel_export import QueryData, ExcelHandler

class NbpApi():
    def query(query_data: QueryData):
        response = requests.get(query_data.query_str)
        if query_data:
            query_data.status = response.status_code
        if response.status_code == 200: return response.json()
        NbpApi._failed_query(response.status_code, query_data)
        return None
        
    def _failed_query(response_status, query_data: QueryData):
        print(f'Failed to get result of a query for {query_data.query_desc}.')
        if response_status == 404:
            pass
        else:
            pass
        counter = 0
        while True:
            print('Would you like to see query string? (Y/N)?')
            user_response = input('>>>').capitalize()
            if user_response == 'Y':
                print(f'Query string is:', query_data.query_str, sep='\n')
            if user_response in ['Y', 'N']: break
            
            counter += 1
            if counter > 10:
                print('OK, you win - no more questions. You can keep your secrets.')
                break

            print(f'Your response "{user_response}" is neither "Y" nor "N". Try again.')

class GoldQueryEnum(Enum):
    CURRENT_PRICE = 1
    TOP_PRICES = 2
    PRICE_ON_DATE = 3
    PRICES_FROM_DATES_RANGE = 4

class GoldPrice(object):
    def __init__(self):
        self.currency = 'PLN'
        self.mass_unit = 'g'
        self.unit = f'[{self.currency}/{self.mass_unit}]'
        self.data_converter = DataConverter(self.mass_unit)
        self.ex_rates_handler = CurrenciesER()
        self.excel_writer = ExcelHandler()
    
    # --- Change reporting parameters ---
    def change_reporting_unit(self):
        self.data_converter.change_base_unit('target')
        self.mass_unit = self.data_converter.target_unit
        self._set_unit()

    def change_reporting_currency(self):
        self.ex_rates_handler.choose_reporting_currency()
        self.currency = self.ex_rates_handler.target_currency
        self._set_unit()

    # --- Queries ---
    def send_query(self, query_option, **kwargs):
        query_obj = QueryData(self.currency, self.mass_unit)
        if query_option == 1:
            html_response = self.current_price(query_obj)
        
        elif query_option == 2:
            html_response = self.top_prices(query_obj, 10)
        
        elif query_option == 3:
            html_response = self.price_on_date(query_obj, kwargs['date'])
        
        elif query_option == 4:
            html_response = self.prices_from_dates_range(query_obj, kwargs['start_date'], kwargs['end_date'])

        else:
            print('There is no such query.')
            return

        if html_response: self._present_query_response(html_response, query_obj)
        self.excel_writer.add_query(vars(query_obj))

    def current_price(self, query_obj):
        query_obj.query_str = 'http://api.nbp.pl/api/cenyzlota'
        query_obj.query_desc = f'Current price of gold {self.unit}'

        return NbpApi.query(query_obj)
    
    def top_prices(self, query_obj, top_count):
        query_obj.query_str = f'http://api.nbp.pl/api/cenyzlota/last/{top_count}'
        query_obj.query_desc = f'Top {top_count} last prices of gold {self.unit}'

        return NbpApi.query(query_obj)
    
    def price_on_date(self, query_obj, date):
        query_obj.query_str = f'http://api.nbp.pl/api/cenyzlota/{date}'
        query_obj.query_desc = f'Prices of gold on {date} in {self.unit}'

        return NbpApi.query(query_obj)
    
    def prices_from_dates_range(self, query_obj, start_date, end_date):
        query_obj.query_str = f'http://api.nbp.pl/api/cenyzlota/{start_date}/{end_date}'
        query_obj.query_desc = f'Prices of gold from {start_date} to {end_date} in {self.unit}'

        return NbpApi.query(query_obj)
    
    # --- Auxiliary functions
    def _present_query_response(self, html_response, query_obj):
        response_handler = ResponseUtils(html_response, self.data_converter.convertion_ratio, self.unit, self.ex_rates_handler)
        data_dict = response_handler.converted_response
        query_obj.add_response_data(data_dict)

        multiple_items = (len(data_dict.keys()) > 1)
        print('\n--- RESPONSE ---\n')
        if multiple_items:
            for counter, date in enumerate(data_dict):
                print(f'{counter + 1}. At {date} the price of gold was {data_dict[date]} {self.unit}.')
        else:
            for date in data_dict:
                print(f'At {date} the price of gold was {data_dict[date]} {self.unit}.')        

        print('\n--- END RESPONSE ---\n')

        if multiple_items:
            if self._ask_if_show_data_stats(): response_handler.get_all_stats()
    
    def _ask_if_show_data_stats(self) -> bool:
        counter = 0
        while True:
            print('\nWould you like to see some more stats on the query results? [Y/N]')
            user_response = input('>>>').capitalize()
            if user_response == 'Y':
                return True
            elif user_response == 'N' or counter > 10:
                return False
            counter += 1

    def _set_unit(self):
        self.unit = f'[{self.currency}/{self.mass_unit}]'

class CurrenciesER(object):
    def __init__(self) -> None:
        self.currencies_table = 'A'
        self.base_currency = 'PLN'
        self.target_currency = 'PLN'
        self.available_currencies = JSONReader.get_currencies_JSON()
        # self._get_available_currencies()      # use only to create or refresh data
    
    def _get_available_currencies(self):
        ''' Auxiliary function used only to prepare JSON file with available currencies info
         as I had no interest in writting in down manually.'''
        query_str = f'http://api.nbp.pl/api/exchangerates/tables/{self.currencies_table}/'
        query_desc = f'Current currency exchange rates data from table {self.currencies_table}'
        html_response = NbpApi.query(query_str, query_desc)
        if not html_response: return
        for data_dict in html_response:
            currencies_dict = {currency_info['code']: currency_info['currency']\
                for currency_info in data_dict['rates']}
        
        JSONReader.write_data_2_JSON(currencies_dict, 'currencies_info')

    def get_currency_ex_rate(self, query_date):
        ''' Function returns target currency ex rate on a given query_date 
        If date is invalid then returns None'''
        if self.target_currency == self.base_currency: return 1
        query_str = f'http://api.nbp.pl/api/exchangerates/rates/{self.currencies_table}/{self.target_currency}/{query_date}/'
        query_desc = f'Target currency ({self.target_currency}) exchange rate on {query_date} from table {self.currencies_table}'
        html_response = NbpApi.query(query_str, query_desc, None)
        if not html_response: return
        return float(html_response['rates'][0]['mid'])

    def _show_available_currencies(self):
        print('Available currencies are:')
        for counter, currency in enumerate(self.available_currencies.items()):
            print(f'{counter + 1}. {currency[0]} - {currency[1]}')
    
    def choose_reporting_currency(self) -> str:
        counter = 0
        while True:
            print('\n--- To choose new reporting currency type its ISO code or type "q" to quit ---\n')
            self._show_available_currencies()
            user_response = input('>>>').upper()

            if self._change_reporting_currency(user_response):
                print(f'--- Successfully set {user_response} as new target currency ---')
                break

            elif user_response != 'Q':
                print(f'''
                {user_response} is not valid currency ISO or it is not on the avaialble currencies list.
                Please try again or type "q" to quit''')

            counter += 1
            if counter > 10 or user_response == 'Q':
                print('--- Failed to change reporting currency ---')
                break
    
    def _change_reporting_currency(self, new_currency: str) -> bool:
        if new_currency in self.available_currencies.keys():
            self.target_currency = new_currency