from nbp_api_app.api_handler import GoldPrice, GoldQueryEnum

golden_query = GoldPrice()

def test_current_gold_price():
    assert golden_query.send_query(GoldQueryEnum.CURRENT_PRICE) == None

def test_gold_top_prices():
    assert golden_query.send_query(GoldQueryEnum.TOP_PRICES) == None

def test_gold_price_on_date():
    test_dates = ['2022-10-10', '2021-03-04', '2049-23-31', 'invalid-date']
    for test_date in test_dates:
        assert golden_query.send_query(GoldQueryEnum.PRICE_ON_DATE, date = test_date) == None

def test_gold_price_in_dates_range():
    test_dates = {
        '2022-10-01': '2022-10-10',
        '2021-02-14': '2021-03-04',
        '2038-01-01': '2049-12-31',
        'not-valid-date': 'not-valid-either'
        }
    for start_date, end_date in test_dates.items():
        assert golden_query.send_query(GoldQueryEnum.CURRENT_PRICE, \
            start_date=start_date, end_date=end_date) == None