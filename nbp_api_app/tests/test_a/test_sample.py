from ...api_handler import CurrenciesER

def is_float(test_val) -> bool:
    try:
        x = float(test_val)
        return True
    except ValueError:
        return False

def test_available_currencies_presentation():
    currencies_er = CurrenciesER()
    assert currencies_er._show_available_currencies() == None