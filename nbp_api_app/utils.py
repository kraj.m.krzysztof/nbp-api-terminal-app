import json, os
from datetime import date
from statistics import median

class ResponseUtils(object):
    def __init__(self, html_response, unit_conversion_ratio, unit, ex_rates_query) -> None:
        self.converted_response = \
            self._convert_list(html_response, unit_conversion_ratio, ex_rates_query)
        self.unit = unit
        
    def get_all_stats(self):
        print('\n--- STATS ---\n')
        print(f'Minimal price was: {self.find_min()} {self.unit}.')
        print(f'Maximal price was {self.find_max()} {self.unit}.')
        print(f'Average price was {self.find_average()} {self.unit}.')
        print(f'Median was {self.find_median()} {self.unit}.')
        print('\n--- END STATS ---\n')

    def find_max(self):
        return max(self.converted_response.values())

    def find_min(self):
        return min(self.converted_response.values())

    def find_average(self):
        return round(sum(self.converted_response.values())
                    / len(self.converted_response.values()), 2)

    def find_median(self):
        return median(self.converted_response.values())

    def _convert_list(self, html_response, conversion_ratio, ex_rates_query):
        ''' Function can be used to convert list of dictionaries {'data': YYYY-MM-DD,
        'cena': p} to dictionary with key:value pairs {YYYY-MM-DD: p}'''
        ex_rate = ex_rates_query.get_currency_ex_rate
        return {dict_data['data']: round(dict_data['cena'] * conversion_ratio / \
            ex_rate(dict_data['data']), 4) for dict_data in html_response}

class DateHandler(object):
    def get_date(earlier_date = '0001-01-01'):
        ''' Function asks user to input date in ISO format. Returns
        date if input validated successfully, else None. If earlier date provided
        function also checks if date is after date passed to the function. '''
        counter = 0
        while counter < 10:
            print('Please provide data in "YYYY-MM-DD" format.')
            user_response = input('>>>')
            if DateHandler._is_date_valid(user_response):
                if not DateHandler._is_it_proper_dates_range(earlier_date, user_response):
                    print(f'Start date {earlier_date} has to be before {user_response}.')
                    return None
                return user_response
            counter +=1
        return None

    def _is_date_valid(date_str) -> bool:
        ''' Function verfies in provided data_str is correct iso format date.
        Also checks if date is from today or past (future dates are not accepted). '''
        if not DateHandler._test_date_format(date_str): return False
        if not DateHandler._is_date_in_the_past(date_str):
            print('Date has to be from the past or today.')
            return False
        return True

    def _test_date_format(date_str) -> bool:
        ''' Test if provided date is in correct ISO format '''
        try:
            test_date = date.fromisoformat(date_str)
            return True
        except ValueError as err:
            print(err)
            return False
    
    def _is_it_proper_dates_range(start_date, end_date) -> bool:
        ''' Function verifies if start date if before end date or the same date. '''
        return (date.fromisoformat(start_date) <= date.fromisoformat(end_date))

    def _is_date_in_the_past(date_to_check: str) -> bool:
        converted_date = date.fromisoformat(date_to_check)
        return (converted_date <= date.today())

    def get_dates_range():
        ''' Function asks user to input start and end date which create dates range.
        Dates range is returned as dictionary with keys ['start_date', 'end_date'] '''
        print('Start date is required.')
        start_date = DateHandler.get_date()
        if not start_date:
            print('No start date provided. Procedure aborted.')
            return None
        else: print(f'Start date as set as {start_date}.', end='\n')

        print('End date is required.')
        end_date = DateHandler.get_date()
        if not end_date:
            print('No end date provided. Procedure aborted.')
            return None
        else: print(f'End date set as {end_date}.', end='\n')
        
        return {'start_date': start_date, 'end_date': end_date}

class DataConverter(object):
    def __init__(self, target_unit, base_unit = 'g') -> None:
        self.base_unit = base_unit
        self.target_unit = target_unit
        self.convertion_ratio = self._get_convertion_ratio()
        self.units_data = JSONReader.get_units_JSON()
    
    def convert_value(self, val: float) -> float:
        ''' Function converts given val from base unit to target unit
        and returns converted value as float '''
        return val * self.convertion_ratio
    
    def _get_convertion_ratio(self) -> float:
        ''' Function converts both base and target unit to grams and calculates conversion ratio '''
        if self.base_unit == self.target_unit: return 1
        return self._mass_in_grams(self.base_unit) * self._mass_in_grams(self.target_unit)
    
    def _mass_in_grams(self, mass_unit: str):
        if mass_unit == 'g': return 1
        unit_data = self.units_data.get(mass_unit)
        if not unit_data: return 0
        return float(unit_data['weight_in_g'])
    
    def change_base_unit(self, unit_type: str) -> None:
        ''' Function allows user to change base or target weight unit.
        Proper arguments for unit_type are 'base' and 'target' '''
        if not unit_type in ['base', 'target']:
            print('Inproper unit type provided.')

        loop_counter = 0
        while True:
            self._print_available_units()
            user_response = input('>>>').lower()
            if not user_response in self.units_data.keys():
                print(f'{user_response} is not among available units.')
            else:
                self._change_converter_unit(unit_type, user_response)
                self.convertion_ratio = self._get_convertion_ratio()
                print(f'Succesfully changed {unit_type} to {self.units_data[user_response]["name"]}.')
                return
            
            loop_counter += 1
            if loop_counter > 10:
                print('You have reached the limit of my patience. Unit not changed.')
                return
    
    def _print_available_units(self) -> None:
        print('Available units are: (choose by entering symbol, i.e. "t" or "ct")')
        for unit in self.units_data.keys():
            print(f'{unit}: {self.units_data[unit]["name"]}.')
    
    def _check_if_unit_available(self, test_unit: str) -> bool:
        if not test_unit in self.units_data.keys():
            print(f'{test_unit} is not among available units.')
        return (test_unit in self.units_data.keys())
    
    def _change_converter_unit(self, unit_type: str, unit: str) -> None:
        if unit_type == 'base':
            self.base_unit = unit
        elif unit_type == 'target':
            self.target_unit = unit
        else:
            print('--- Invalid unit type, no change made ---')

    
class JSONReader(object):
    def get_units_JSON():
        f = open('nbp_api_app/static/JSON/mass_convertion_ratios.json')
        return json.load(f)
    
    def get_currencies_JSON():
        f = open('nbp_api_app/static/JSON/currencies_info.json')
        return json.load(f)
    
    def write_data_2_JSON(data_dict, file_name):
        ''' Function saves dictionary data_dict as JSON file in static/JSON folder.
        It is not necessary to add .json to file_name as it's added to the saving path.'''
        file_path = 'nbp_api_app/static/JSON/' + file_name + '.json'
        json_obj = json.dumps(data_dict, indent=4)

        with open(file_path, "w") as outfile:
            outfile.write(json_obj)