import os
import pandas as pd

class QueryData(object):
    def __init__(self, currency: str, unit: str) -> None:
        self.query_str = ''
        self.query_desc = ''
        self.status = 0
        self.query_currency = currency
        self.query_unit = unit
        self.query_response = []
    
    def add_response_data(self, data):
        self.query_response = data

class ExcelHandler(object):
    def __init__(self) -> None:
        self.queries = []
    
    def add_query(self, query_data):
        self.queries.append(query_data)
    
    def __str__(self) -> str:
        obj_str = ''
        for i, query in enumerate(self.queries):
            obj_str += '\n' + f'{i + 1}. Query: {query}'
        return obj_str

    def save_queries(self):
        ''' Function asks user if wishes to save queries data to Excel spreadsheet
        (only if queries were made). '''
        if not len(self.queries) > 0: return
        print('\n--- QUERY SAVER START ---\n')

        if self._ask_if_save_queries():
            self.create_excel_with_queries_data()

        print('\n--- QUERY SAVER END ---\n')
    
    def _ask_if_save_queries(self) -> bool:
        counter = 0
        while True:
            counter += 1
            print('Would you like to save your queries data as an Excel spreadsheet? [Y/N]')
            user_response = input('>>>').upper()
            if user_response == 'Y':
                return True

            elif user_response == 'N':
                return False

            elif counter > 10:
                print('Enough of this nonsense!')
                return False

            print(f'Your response "{user_response}" is neither in [Y/N] nor fun. Try again!')

    def create_excel_with_queries_data(self):
        wss_df = self.write_queries_details()
        main_ws_df = self.write_main_ws()

        file_path = os.path.join(os.getcwd(), 'nbp_api_app', 'static', 'Excel', 'queries.xlsx')

        with pd.ExcelWriter(file_path) as writer:
            main_ws_df.to_excel(writer, sheet_name='Queries')
            for i, ws_df in enumerate(wss_df):
                ws_df.to_excel(writer, sheet_name=f'Query {i + 1}')
        print(f'--- Successfully saved {len(wss_df)} queries to {file_path}. ---')

    def write_main_ws(self):
        queries_copy = self.queries
        data_dict = {i + 1: query for i, query in enumerate(queries_copy)}
        return pd.DataFrame.from_dict(data_dict, orient='index')

    def write_queries_details(self) -> pd.DataFrame:
        return [self._single_query_details(query) for query in self.queries]
    
    def _single_query_details(self, query_data):
        response_dict = query_data.pop('query_response')
        new_dict = {i + 1: {'date': key, 'price': response_dict[key]} \
            for i, key in enumerate(response_dict)}
        return pd.DataFrame.from_dict(new_dict, orient='index')
